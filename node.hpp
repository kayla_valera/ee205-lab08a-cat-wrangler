///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file node.hpp
/// @version 1.0
///
/// A generic Node class.  May be used as a base class for a Doubly Linked List
///
// @author Kayla Valera <kvalera@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   @todo 13_APR_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

class Node {
friend class SingleLinkedList;

protected:
	Node* next = nullptr;

public:
	virtual bool operator>(const Node& r);

}; // class Node
