///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file queue.hpp
/// @version 1.0
///
/// A generic Queue collection class.
///
/// @author Kayla Valera <kvalera@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   @todo 13_APR_2021
///////////////////////////////////////////////////////////////////////////////

#include "node.hpp"
#include "list.hpp"

#pragma once

class Queue {
protected:
	DoubleLinkedList list = DoubleLinkedList();

public:
	inline const bool empty() const { return list.empty(); };

	// Using the above as an example, bring over the following methods from list:
	// Add size()
	// Add push_front()
	// Add pop_back()
	// Add get_first()
	// Add get_next()
 	inline const unsigned int size() const {return list.size();};
    	inline const void push_front( Node* newNode ) const {return push_front( newNode );};
	inline Node* get_first() const {return list.get_first();};
	inline Node* get_next(const Node* currentNode) const {return list.get_next(currentNode);};

       	inline const Node* pop_back() const {return pop_back();};
       
}; // class Queue
