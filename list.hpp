///////////////////////////////////////////////////////////////////////////////
///// University of Hawaii, College of Engineering
///// EE 205  - Object Oriented Programming
///// Lab 07a - Animal Farm 4
/////
///// @file list.hpp
///// @version 1.0
/////
///// DoubleLinkedList.
/////
///// Kayla Valera <kvalera@hawaii.edu>
///// @brief  Lab 08a - CatWrangler - EE 205 - Spr 2021
///// @date   13_APR_2021
/////////////////////////////////////////////////////////////////////////////////



#pragma once							  
#include "node.hpp"
															  
											class DoubleLinkedList {
											protected:
												Node* head = nullptr;
												unsigned int count = 0;
																				
											public:
												const bool empty() const ;
												void push_front( Node* newNode );
												Node* pop_front() ;
												unsigned int size() const;

											Node* get_first() const;
	Node* get_next( const Node* currentNode ) const ;

											};

								
