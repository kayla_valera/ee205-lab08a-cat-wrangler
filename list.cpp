///////////////////////////////////////////////////////////////////////////////
///// University of Hawaii, College of Engineering
///// EE 205  - Object Oriented Programming
///// Lab 08a - Cat Wrangler
/////
///// @file list.cpp
///// @version 1.0
/////
///// DoubleLinkedList
/////
///// Kayla Valera <kvalera@hawaii.edu>
///// @brief  Lab 08a - CatWrangler - EE 205 - Spr 2021
///// @date   13_ APR_2021
/////////////////////////////////////////////////////////////////////////////////


#include "list.hpp"

const bool DoubleLinkedList::empty() const {
		return (head == nullptr);
}


void DoubleLinkedList::push_front( Node* newNode ) {
		if( newNode == nullptr )
		return;  
			
			newNode->next = head;
				head = newNode;
					
					count++;
}


Node* DoubleLinkedList::pop_front() {
	if( head == nullptr )
	return nullptr; 

	Node* returnValue = head;
				
	head = head->next;
					
	count--;
						
	return returnValue;
}


Node* DoubleLinkedList::get_first() const {
		return head;
}


Node* DoubleLinkedList::get_next(const Node* currentNode) const {
		return currentNode->next;
}


unsigned int DoubleLinkedList::size() const {
		return count;
}

